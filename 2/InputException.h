#pragma once
#include <iostream>
#include <String>

class InputException :public std::exception
{
private:
	std::string _input;
public:
	virtual const char* what() const;
	InputException(std::string input);
	~InputException();
	void setInput(std::string input);
};

InputException::InputException(std::string input)
{
	_input = input;
}

InputException::~InputException()
{}
const char* InputException::what() const
{
	bool isNum;
	if (_input[0] == '-')
		throw std::string("ERROR - you entered negative value\n");
	else
	{
		int i = 0;
		for (i = 0; i < _input.length(); i++)
		{
			if(!isdigit(_input[i]) &&_input[i] != '.')
				throw std::string("ERROR - you entered letteres instead numbers\n");
		}
	}
}

void InputException::setInput(std::string input)
{
	_input = input;
}

