#include "MathUtils.h"
MathUtils::MathUtils(double fin):_fin(fin)
{}
//i read about the calculates in wiki
double MathUtils::CalPentagonArea(double fin)
{
	return 1.72048 * pow(fin, 2);
}

double MathUtils::CalHexagonArea(double fin)
{
	return 2.59808 * pow(fin, 2);
}