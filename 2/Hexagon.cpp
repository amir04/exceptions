#include "Hexagon.h"
Hexagon::Hexagon(std::string nam, std::string col, double fin):Shape(nam, col)
{
	setFin(fin);
}
void Hexagon::setFin(double fin)
{
	_fin = fin;
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "fin length is " << _fin << std::endl << "Area is: " << CalArea()<<std::endl;
}

double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(_fin);
}