#pragma once
#include <vector>
// i use vecter to get the function pow
class MathUtils
{
public:
	//constractor and area function (5 or 6)
	MathUtils(double fin);
	static double CalPentagonArea(double fin);
	static double CalHexagonArea(double fin);
private:
	double _fin;
};
