
#include <iostream>
#include <string>
using namespace std;

int add(int a, int b) {
  	if (a + b == 8200)
		//if its 8200 i throw string and catch it in the main
		throw string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year�.");
  return a + b;
}

int  multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  return sum;
}

int  pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  return exponent;
}

int main(void) {
	try {
		cout<<pow(4000, 2) << "\n";
	}
	catch (string s) {
		cout << "multiply: " +s + "\n";
	}
	try {
		cout << multiply(2050, 8) << "\n";
	}
	catch (string s) {
		cout << "pow: " + s + "\n";
	}
	getchar();
}  
