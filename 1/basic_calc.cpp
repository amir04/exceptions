#include <iostream>
int add(int a, int b, bool * notOkay) {
	//if its 8200 i change the bool to true- (pointer) and than in the main i will print the error
	if ((a + b) == 8200)
		*(notOkay) = true;
	return a + b;
}

int  multiply(int a, int b, bool* notOkay) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, notOkay);
  }
  return sum;
}

int  pow(int a, int b, bool* notOkay) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, notOkay);
  };
  return exponent;
}

void PrintAnswer(int result, bool notOkay) {
	if (!notOkay)
		std::cout << result;
	else
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year�.";
	std::cout << "\n";
}

int main(void) {
	bool notOkay = false;
	int answer = pow(1, 1, &notOkay);
	PrintAnswer(answer, notOkay);

	notOkay = false;
	answer = multiply(4100, 2, &notOkay);
	PrintAnswer(answer, notOkay);
	getchar();

}
